import math
import sys

def process_input_file(training):
    """
    Reads the input training data file, finds the statistics of the words
    in the input file which would be used for generating model file.
    @param training: name of the training file for learning the training data.
    """
    training_data = {}
    # , encoding="utf8"
    with open(training, "r") as f_in:
        for line in f_in.readlines():
            words = line.strip().split(" ")
            words = [w.strip() for w in words if len(w.strip()) > 0]
            cls = words[0]
            if cls in training_data:
                training_data[cls]["_ccount"] += 1
            else:
                training_data[cls] = {}
                # Save count of no. of documents of this class.
                training_data[cls]["_ccount"] = 1
                # Save count of no. of words in this class.
                training_data[cls]["_wcount"] = 0
            for w in words[1:]:
                training_data[cls]["_wcount"] += 1
                if w in training_data[cls]:
                    training_data[cls][w] += 1
                else:
                    training_data[cls][w] = 1
    return training_data

def generate_model_file(model_file, data):
    """
    Saves the data characteristics of the training data to a model file
    which would be used by the classifier to classify new files
    @param model_file: file name to which data model contents are saved.
    @param data: dict of words and their corresponding count as values present in each class.
    """
    # Get the vocab size of all the classes.
    all_words = []
    for cls in data:
        all_words.extend(list(data[cls].keys()))
    unique_words =list(set(all_words))
    unique_words.remove("_wcount")
    unique_words.remove("_ccount")
    
    #, encoding="utf8"
    with open(model_file, "w") as f_out:
        # Total size of the vocab of all classes.
        total_vocab_size = len(unique_words)
        f_out.write(str(total_vocab_size) + "\n")
        # Write the total no. of classes.
        f_out.write(str(len(data.keys())) + "\n")
        for cls in data:
            # class, no. of documents and total word count that belongs to this class.
            f_out.write(cls + " " + str(data[cls]["_ccount"]) + " " + str(data[cls]["_wcount"]) + "\n")
            class_vocab_size = len(data[cls].keys()) - 2 # Remove "count" and "wcount"
            # Size of vocabulary of this class i.e. no. of unique words in this class.
            f_out.write(str(class_vocab_size) + "\n")
            # Write the word and its probability with add-one smoothing.
            total_words = data[cls]["_wcount"]
            for w in data[cls]:
                if w in ("_wcount", "_ccount"):
                    continue
                else:
                    probability = (data[cls][w] + 1)/(total_words + total_vocab_size)
                    f_out.write(w + " " + str(math.log(probability)) + "\n")


def main(training_file, model_file):
    """
    @param training_file: input file which contains training data.
    @param model_file: output file which contains the data for classifier
    """
    data = process_input_file(training_file)
    generate_model_file(model_file, data)
    #print ("Done with nblearn")

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    #main("TRAININGFILE_SENTI", "sentiment.nb")
