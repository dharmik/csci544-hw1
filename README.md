1) Generate_input_file.py generates the training or test file <output_file> for a given set of documents which resides inside the directory <data_dir>. The type of the output file can be set using the <is_training_file> argument. If this argument is 1, the output file contains document label (the class the document belongs to) as the first column(eg.: HAM or SPAM) which is extracted from the file names and if argument is 0, a test file is generated without labels in the first column. Check handout for the format of <output_file> which should be used as input to the scripts (nb*.py)

$> python3  generate_input_file.py <data_dir> <output_file> <is_training_file>


2) nblearn.py learns the classifier using Naive Bayes principle and takes the <training_file> as 1st argument for reading and <model_file> as the second argument for writing the learned model which would be used by classifier to classify the new documents.

$> python3 nblearn.py <training_file> <model_file>


3) nbclassify.py script classifies the files listed in <test_file> making use of <model_file> and prints the output to console.

$> python3 nbclassify.py <model_file> <test_file>


4) generate_file_order.py generates a reference file <output_file> which contains the labels of the files present inside <data_dir>. This reference file is used to calculate the accuracy with the output file generated from classifier script.

$> python3  generate_file_order.py <data_dir> <output_file>
=================== PART2 =======================

*****SVM-light*****
Using of SVM-light requires formatting the input file as specified in http://www.cs.cornell.edu/People/tj/svm_light/ 
I have made use of tf-idf values for corresponding features which are stored in "metadata_file" while formatting the input file to desired file format.

NOTE: Make sure that the below <svm_training_file> and <svm_test_dev_file> are generated one after the other so that the contents of metadata_file is not changed due to some other process.

The steps to use the SVM-light package is given below:

$> python3 ip_for_svm.py <training_file> <svm_training_file>

$> python3 test_for_svm.py <test_dev_file> <svm_test_dev_file>

$> svm_learn <svm_training_file> <svm_model_file>

$> svm_classify <svm_test_dev_file> <svm_model_file> <svm_output_file>

$> python3 transform_svm_op.py <svm_output_file> <output_file_with_labels>

$> python3 get_scores.py <ref_file> <output_file_with_labels>

In additon, I have also written ip_svm_count.py and test_simple_svm.py which just takes the count of features rather than the TF-IDF values.

Please note that the accuracy for tf-idf values is more than just the count values.

***** MegaM *****

The format of the input file is mentioned in http://www.umiacs.umd.edu/~hal/megam/

NOTE: make sure that the input files - training and test files are generated one after the other so that the megaM_metadata file is not corrupted due to other process execution.

The steps to use the MegaM package is given below:

>$ python3 get_megam_input.py sentiment_train.txt sentiment.megam.train 0

>$ python3 get_megam_input.py sentiment_test.txt sentiment.megam.test 2

>$ megam -nc multiclass sentiment.megam.train > sentiment.megam.model

>$ megam -predict sentiment.megam.model multiclass sentiment.megam.test > sentiment.megam.pred

>$ python3 transform_megam_op.py sentiment.megam.pred sentiment.megam.out

>$ python3 ../get_scores.py sentiment_dev_ref.txt sentiment.megam.dev.out


=================== PART-3 ======================

******NAIVE BAYES******
SPAM - HAM
Correct= 1342  - Incorrect= 21
Accuracy =  0.9845928099779897

**HAM**
Precision = 0.9949443882709808
Recall = 0.984
F-score = 0.989441930618401

**SPAM**
Precision = 0.95721925133689839572192513368984
Recall = 0.98622589531680440771349862258953
F-score = 0.9715061058344641

{('SPAM', 'HAM'): 5, ('SPAM', 'SPAM'): 358, ('HAM', 'SPAM'): 16, ('HAM', 'HAM'): 984}

***SENTIMENT_DEV***
POS - NEG
Correct= 4745  - Incorrect= 255
Accuracy =  0.949

**POS**
Precision = 0.9651885619560713
Recall = 0.9316
F-score = 0.9480966146981107

**NEG**
Precision = 0.9339002705836876
Recall = 0.9664
F-score = 0.9498722233143307
{('POS', 'POS'): 2329, ('POS', 'NEG'): 171, ('NEG', 'NEG'): 2416, ('NEG', 'POS'): 84}

******SVM-light******
SPAM - HAM
Correct= 1326  - Incorrect= 37
Accuracy =  0.9728539985326485

**HAM**
Precision = 0.9670223084384093
Recall = 0.997
F-score = 0.9817823732151649

**SPAM**
Precision = 0.9909638554216867
Recall = 0.90633608815427
F-score = 0.9467625899280576
{('HAM', 'SPAM'): 3, ('SPAM', 'HAM'): 34, ('HAM', 'HAM'): 997, ('SPAM', 'SPAM'): 329}

***SVM-light SENTIMENT***
POS - NEG
Correct= 4894  - Incorrect= 106
Accuracy =  0.9788

**POS**
Precision = 0.9765127388535032
Recall = 0.9812
F-score = 0.978850758180367

**NEG**
Precision = 0.9811093247588425
Recall = 0.9764
F-score = 0.9787489975942263

{('NEG', 'POS'): 59, ('POS', 'NEG'): 47, ('POS', 'POS'): 2453, ('NEG', 'NEG'): 2441}

******MegaM - SPAM CORRECTNESS******
Correct= 1241  - Incorrect= 122
Accuracy =  0.9104915627292737

**HAM**
Precision = 0.9262135922330097
Recall = 0.954
F-score = 0.9399

**SPAM**
Precision = 0.8618618618618619
Recall = 0.790633608815427
F-score = 0.824712643678161

{('SPAM', 'SPAM'): 287, ('HAM', 'SPAM'): 46, ('HAM', 'HAM'): 954, ('SPAM', 'HAM'): 76}

***SENTIMENT***
Correct= 3450  - Incorrect= 1550
Accuracy =  0.69

**POS**
Precision = 0.6941946034341783
Recall = 0.6792
F-score = 0.6866

**NEG**
Precision = 0.6859827721221613
Recall = 0.7008
F-score = 0.6933122279382666

{('POS', 'NEG'): 802, ('NEG', 'POS'): 748, ('POS', 'POS'): 1698, ('NEG', 'NEG'): 1752}

If only 10% of the training data is used to train the classifiers, then we get lower values of Precision, Recall and F-scores because the model may not correctly learn from the data and incorrectly classifies the dev data. Thus if we have more training data, the probability of classifier to correctly classify the test daa increases which inturn increases the scores.
