import sys

def read_meta_data():
    f_in = open("metadata_file", "r")
    class_type = {}
    class_type[-1] = f_in.readline().strip()
    class_type[1] = f_in.readline().strip()
    return class_type
    

def main(input_file, output_file):
    class_val = read_meta_data()
    f_in = open(input_file, "r")
    f_out = open(output_file, "w")
    for line in f_in.readlines():
        val = float(line.strip())
        if val < 0:
            f_out.write(class_val[-1] + "\n")
        else:
            f_out.write(class_val[1] + "\n")
    f_in.close()
    f_out.close()
        

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])