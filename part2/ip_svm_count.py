import math
import sys
CLASS_TYPE = {}
CLASS_TYPE_LIST = []
COUNTER = -1
DOC_COUNT = 0
VOCAB = {}
VOCAB_LIST = []
VOCAB_INDEX = 2 # 0 and 1 are used as class labels.
DOC_FREQUENCY = {}


def read_input_file(file_name):
    global CLASS_TYPE, COUNTER, DOC_COUNT, VOCAB, VOCAB_INDEX, DOC_FREQUENCY, VOCAB_LIST, CLASS_TYPE
    file_contents = []
    try:
        ip_file = open(file_name, "r", errors = "ignore")
        for line in ip_file.readlines():
            DOC_COUNT += 1
            doc_content = {}
            words = line.strip().split(" ")
            words = [w.strip() for w in words if len(w.strip()) > 0]
            if not words[0] in CLASS_TYPE:
                if COUNTER > 1:
                    print ("More than 2 classses are found in training file!")
                    print ("SVM-light can only handle binary classification. Exiting now...")
                    exit()
                CLASS_TYPE[words[0]] = COUNTER
                COUNTER += 2
                CLASS_TYPE_LIST.append(words[0])
            doc_content[-1] = CLASS_TYPE[words[0]]
            for w in words[1:]:
                # Make sure an index exists for a word. If not, then give it an index.
                if not w in VOCAB:
                    VOCAB_INDEX += 1
                    VOCAB[w] = VOCAB_INDEX
                    VOCAB_LIST.append(w)
                vocab_index = VOCAB[w]
                # After having a word index, keep a TF (count of word in that doc)
                if vocab_index in doc_content:
                    doc_content[vocab_index] += 1
                else:
                    doc_content[vocab_index] = 1
            file_contents.append(doc_content)
        ip_file.close()
        return file_contents
    except Exception as e:
        print (e)
        print ("Exiting now...")
        exit()

def write_output_file(output_file, file_contents):
    vocab_file = open("metadata_file", "w", errors="ignore")
    for c in CLASS_TYPE_LIST:
        vocab_file.write(c + "\n")
    for w in VOCAB_LIST:
        vocab_file.write(w+ "\n")
    vocab_file.close()
    with open(output_file, "w", errors="ignore") as op_file:
        for map in file_contents:
            op_file.write(str(map[-1]))
            for k in sorted(map.keys()):
                if k != -1:
                    op_file.write(" " + str(k) + ":" + str(map[k]))
            op_file.write("\n")

def main(input_file, output_file):
    file_contents = read_input_file(input_file)
    print ("Done with reading files: ", len(file_contents))
    write_output_file(output_file, file_contents)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
# Index for distinct words in word_file
#dict of word and index

# dict for each line to maintain count of words.
# maintain a dict for document frequency

