import sys

VOCAB = {}
CLASS_TYPE = {}
FEATURE_VAL = {}

def get_meta_data():
    f_in = open("metadata_file", "r", errors="ignore")
    global VOCAB, FEATURE_VAL
    global CLASS_TYPE
    line = f_in.readline().strip()
    CLASS_TYPE[line] = -1
    line = f_in.readline().strip()
    CLASS_TYPE[line] = 1
    line_counter = 3
    for line in f_in.readlines():
        #print (line_counter)
        word = line.strip()
        VOCAB[word] = line_counter
        #FEATURE_VAL[line_counter] = float(words[1])
        line_counter += 1
    f_in.close()
    

def read_input_file(input_file):
    f_in = open(input_file, "r", errors="ignore")
    file_content = []
    for line in f_in.readlines():
        words = line.strip().split(" ")
        line_content = {}
        for w in words :
            # Ignore new words for now.
            try:
                feature = VOCAB[w]
                if feature in line_content:
                    line_content[feature] += 1
                else:
                    line_content[feature] = 1
            except KeyError:
                continue
        file_content.append(line_content)
    f_in.close()
    return file_content

def generate_test_file(output_file, file_content):
    f_out = open(output_file, "w", errors="ignore")
    for map in file_content:
        f_out.write("0 ")
        for k in sorted(map.keys()):
            f_out.write(" " + str(k) + ":" + str(map[k]))
        f_out.write("\n")
    f_out.close()

def main(input_file, output_file):
    get_meta_data()
    file_content = read_input_file(input_file)
    generate_test_file(output_file, file_content)

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
