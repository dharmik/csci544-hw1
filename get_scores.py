import sys

def main(ip_file, op_file):
    f_in = open(ip_file,"r", errors="ignore")
    f_out = open(op_file,"r", errors="ignore")
    ip_list = [l.strip() for l in f_in.readlines() if len(l.strip()) > 0]
    op_list = [l.strip() for l in f_out.readlines() if len(l.strip()) > 0]
    if len(ip_list) != len(op_list):
        print ("Different number of lines in given files", len(ip_list), len(op_list))
    ip_classes = list(set(ip_list))
    op_classes = list(set(op_list))
    class_pair = []
    cmp_dict = {}
    for c1 in ip_classes:
        for c2 in op_classes:
            class_pair.append((c1,c2))
            cmp_dict[(c1,c2)] = 0
    correct = 0
    incorrect = 0
    for i in range(0, len(ip_list)):
       cmp_dict[(ip_list[i],op_list[i])] += 1
       if ip_list[i] == op_list[i]:
           correct += 1
       else:
           incorrect += 1
    print ("Correct=", correct, " - Incorrect=", incorrect)
    print ("Accuracy = ", correct/(correct+incorrect))
    print (cmp_dict)
    
        

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
