import math
import sys

TOTAL_DOCS = 0
UNKNOWN_PROB = {}
CLASS_PROB = {}

def read_model_file(modelfile):
    """
    Reads the model file and gets the required information to
    classify the new files.
    The format of the model file is as follows:
    1) <vocab_size> # No. of unique words in all the classes.
    2) <no. of classes> # Number of distinct classes the document can belong to.
    3) <class_name> <no. of class docs> <no. of words in this class>
    4) <no. of unique words in this class>
    5) Next 'n' lines contains word and its probability that it belongs to this class.
           'n' is defined in line 4.
    ('n' + 4) line contains  <class_name> <no. of class docs> <no. of words in this class>
    """
    data_stats = {}
    with open(modelfile, "r") as f_in:
        vocab_size = int(f_in.readline().strip())
        no_of_classes = int(f_in.readline().strip())
        for x in range(0, no_of_classes):
            cls_details = f_in.readline().strip().split(" ")
            class_name = cls_details[0]
            class_count = int(cls_details[1])
            class_wcount = int(cls_details[2])
            data_stats[class_name] = {"_ccount": class_count, "_wcount" : class_wcount}
            unique_words = int(f_in.readline().strip())
            for y in range(0, unique_words):
                word, log_probability = f_in.readline().strip().split(" ")
                data_stats[class_name][word] = float(log_probability)
    return (vocab_size, data_stats)

def pre_compute_globals(vocab_size, data_stats):
    global TOTAL_DOCS, UNKNOWN_PROB, CLASS_PROB
    for cls in data_stats:
        TOTAL_DOCS += data_stats[cls]["_ccount"]
    for cls in data_stats:
        CLASS_PROB[cls] = math.log(data_stats[cls]["_ccount"] / TOTAL_DOCS)
        UNKNOWN_PROB[cls] = math.log(1/(data_stats[cls]["_wcount"] + vocab_size))


def get_class_for_msg(msg, vocab_size, data_stats):
    """
    This method does most of the computation required to compute the class
    and returns that class which has highest probability.
    """
    words = msg.split(" ")
    words = [w for w in words if len(w) > 0]
    max_prob = -99999999999
    computed_class = ""
    for cls in data_stats:
        log_prob_word_class = 0
        for w in words:
            # Either get the probability f present else get the
            # default probability of add-1 smoothing
            log_prob_word_class += data_stats[cls].get(w, UNKNOWN_PROB[cls])
        probability = log_prob_word_class + CLASS_PROB[cls]
        #print (cls, probability )
        if max_prob < probability:
            max_prob = probability
            computed_class = cls
    return computed_class

def compute_class_for_testfile(vocab_size, data_stats, testfile):
    """
    Reads the input test file line by line and and tries to get the class for each
    test case using naive bayes probability.
    """
    with open(testfile, "r", errors = "ignore") as f_in:
        for line in f_in.readlines():
            msg = line.strip()
            computed_class = get_class_for_msg(msg, vocab_size, data_stats)
            print (computed_class)

def main(modelfile, testfile):
    vocab_size, data_stats = read_model_file(modelfile)
    #print (vocab_size)
    #for cls in data_stats:
    #    print (cls, data_stats[cls]["_wcount"], data_stats[cls]["_ccount"])
    #    print (len(data_stats[cls].keys()) - 2)
    pre_compute_globals(vocab_size, data_stats)
    compute_class_for_testfile(vocab_size, data_stats, testfile)
    #print ("Done computing the classes for the test file")

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    #main("sentiment.nb", "TESTFILE_SENTI")
