from os import listdir
from os.path import isfile, join, basename
import sys


def get_input_files(data_dir):
    try:
        #train_data_dir_path = "E:/USC/NLP/hws/hw1/SPAM_dev"
        #train_data_dir_path = "E:/USC/NLP/hws/hw1/SENTIMENT_dev"
        file_list = [data_dir + "/" +f for f in listdir(data_dir) if isfile(join(data_dir, f))]
        file_list.sort()
        print ("No. of files in training dir = ", len(file_list))
        #print (file_list[4000:4100])
        return file_list
    except FileNotFoundError as e:
        print ("Invalid directory path mentioned")
        print (e)
        exit()

def generate_training_file(file_list, op_file):
    with open(op_file, "w", encoding="utf8", errors="ignore") as f_out:
    #with open(op_file, "w", errors="ignore") as f_out:
        for f in file_list:
            file_class = basename(f)
            file_class = file_class.split(".")[0]
            f_out.write(str(file_class) + "\n")


def main(data_dir, op_file):
    files = get_input_files(data_dir)
    #print (files)
    generate_training_file(files, op_file)
    print ("Done generating the training data")

if __name__ == "__main__":
    #print (sys.argv[1], sys.argv[2]) 
    main(sys.argv[1], sys.argv[2])
