from os import listdir
from os.path import isfile, join, basename
import sys


def get_input_files(data_dir):
    try:
        file_list = [data_dir + "/" +f for f in listdir(data_dir) if isfile(join(data_dir, f))]
        file_list.sort()
        print ("No. of files in training dir = ", len(file_list))
        #print (file_list[4000:4100])
        return file_list
    except FileNotFoundError as e:
        print ("Invalid directory path mentioned")
        print (e)
        exit()

def get_formatted_file_content(file_name):
    bag_of_words = []
    try:
        f_in = open(file_name, "r", encoding="utf8", errors="ignore")
        #f_in = open(file_name, "r", errors="ignore")
        for line in f_in.readlines():
            words = line.strip().split(" ")
            non_empty_words = [w for w in words if len(w) > 0]
            bag_of_words.extend(non_empty_words)
    except Exception as e:
        print ("Error reading input file : ", file_name)
        print (e)
    #print (file_name, "----", len(bag_of_words))
    #if "\n" in bag_of_words:
    #    print ("New line!!!")
    return " ".join(bag_of_words)

def generate_training_file(file_list, op_file, is_training_file):
    with open(op_file, "w", encoding="utf8", errors="ignore") as f_out:
    #with open(op_file, "w", errors="ignore") as f_out:
        for f in file_list:
            file_class = basename(f)
            file_class = file_class.split(".")[0]
            if is_training_file== "1":
                f_out.write(str(file_class) + " " + get_formatted_file_content(f) + "\n")
            else:
                f_out.write(get_formatted_file_content(f) + "\n")

def main(data_dir, op_file, is_training_file):
    files = get_input_files(data_dir)
    #print (files)
    generate_training_file(files, op_file, is_training_file)
    print ("Done generating the training data")

if __name__ == "__main__":
    #print (sys.argv[1], sys.argv[2]) 
    main(sys.argv[1], sys.argv[2], sys.argv[3])
